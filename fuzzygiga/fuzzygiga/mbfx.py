# -*- coding: utf-8 -*-

 
#Fuzzy Logic Pack for Python 2.7.x 
#
#	Author: 
#		2015 (C) Ing. Julian Andres Guarin Reyes (C) 2015.
#		2015 (C) Fitech Giga S.A.S.
#		
#		
#							
#  This software is provided 'as-is', without any express or implied
#  warranty. In no event will the authors be held liable for any damages
#  arising from the use of this software.

#  Permission is granted to anyone to use this software for any purpose,
#  including commercial applications, and to alter it and redistribute it
#  freely, subject to the following restrictions:

#    1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.

#    2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.

#    3. This notice may not be removed or altered from any source
#    distribution.
#    
#    mbfx is the base class for membership functions.


import sys
import numpy as np
import random
import matplotlib.pyplot as plt

class randomname(object):
	
		
	def __init__ (self,name=None):
		self._name_suffix_dict = {0:'NotAName'}
	def suffix_name(self,name):
		idx = 0
		while idx in self._name_suffix_dict:
			idx = random.randint(0x1000,0xffffff) 
		self._name_suffix_dict[idx]=name+'_'+hex(idx)[2:]
		return self._name_suffix_dict[idx]
mbfx_names = randomname()

class mbfx(object):


	@property
	def dominion_feature(self):
	    return self._dominion_feature
	
	@dominion_feature.setter
	def dominion_feature(self,value):

		if type(value) == str:
			value = '_'+value 
			try:
				self._dominion_feature.append(value)
			except:
				self._dominion_feature=[value]
		elif type(value) == list:
			for idx, val in enumerate(value):
				value[idx] = '_'+val
			try:
				self._dominion_feature+=value
			except:
				self._dominion_feature=value



	@property
	def debugging(self):
	    return self._debugging
	@debugging.setter
	def debugging(self, value=False):
		if type(value) != bool:
			self._debugging = False
		else:
			self._debugging = value
	
	@property
	def node_density(self):
		return self._node_density
	@node_density.setter
	def node_density(self,value):
		self._node_density = value

	@property
	def xmin(self):
		return self._xmin
	@xmin.setter
	def xmin(self,value):
		self._xmin = value

	@property
	def xmax(self):
		return self._xmax
	@xmax.setter
	def xmax(self,value):
		self._xmax = value

	@property
	def name(self):
		try:
			return self._name
		except:
			self._name = str(self.__class__)
			return self._name
	@name.setter
	def name(self,value):
		self._name = value		
	
	@property
	def w(self):
	    return self._w
	@w.setter
	def w(self, w):
		self._w = float(w)


	@property
	def evaluation_type(self):
	    return self._evaluation_type
	@evaluation_type.setter
	def evaluation_type(self, eval_type):
		self._evaluation_type = eval_type

	def get_dominion (self, margin = None):

		dom_lenght = self.xmax-self.xmin
		
		if margin == None:
			margin = dom_lenght*.20
		
		dom = np.linspace(-margin+self.xmin,self.xmax+margin,self.node_density)
		dom_list = dom.tolist()
		
		for feature in self.dominion_feature:
			#check feature IS in the class and IS a string
			try:
				assert(type(feature)==str or '_'+feature in self.__dict__)
			except AssertionError:
				print "Assertion Error: ",feature, " ",type(feature)," ",self.__dict__
			#check value is in the dominion interval and is not in the dominion list values
			try:
				x = self.__dict__[feature]
			except KeyError:
				print feature
				print (self)
				print self.__dict__.keys()
				print self._dominion_feature
				sys.exit()

			if (x>=self.xmin) and (x<=self.xmax) and (x not in dom_list):
				dom_list.append(x)
		
		#rearrange list and rebuild dominion		
		dom_list.sort()
		dom = np.array(dom_list)
		return dom



	def mbfx(self,membership=None, x=None):
		if membership==None: 		membership = 0
		if type(membership)==bool:	membership = float(membership)
		elif membership>1.0: 		membership = 1.0
		elif membership<0.0: 		membership = 0.0
		else:						membership = float(membership)

		if self._evaluation_type == 'sugeno':
			return (1 - membership) / ( 1 + self._w*membership)
		elif self._evaluation_type == 'yager':
			return (1 - (membership**self._w))**(1/self._w) 
		else:
			return membership





	



	
	def __init__(self, xmin=0.0, xmax=1.0, name='Ambfx'):

		self._w = 0.2
		self.xmin = xmin
		self.xmax = xmax
		self.debugging = False
		self.node_density = 1000
		self._evaluation_type = 'normal'
		self.dominion_feature = ['xmin','xmax']
		self.name = mbfx_names.suffix_name(name)

	def plot_mbfx(self, format_string='b_', **kwargs):

		ndots	= (self.xmax-self.xmin) * self.node_density
		dom		= np.linspace(self.xmax,self.xmin,ndots) 
		ran     = []

		for x in dom:
			ran.append(self.mbfx(x))

		ran 			= np.array(ran)
		ran_height		= ran.max()-ran.min()
		ymin			= ran.min()-.20*ran_height
		ymax			= ran.max()+.20*ran_height
		xmin 			= dom.min()
		xmax 			= dom.max()

		f,ax = plt.subplots()
		print(f,ax)
		ax.plot(dom,ran,format_string,**kwargs)
		plt.xlim(xmin,xmax)
		plt.ylim(ymin,ymax)
		plt.ylabel("Pertenencia")
		plt.grid(which='major',axis='both',color='g')
		plt.title(self.name)

		f.show()

	def fastplot(self, margin=None):

		
		dom = self.get_dominion(margin)
		ran = []

		for x in dom:
			val = self.mbfx(x)
			
			if val>1.0:
				val = 1.0
			elif val<0.0:
				val = 0.0
			ran.append(val)
		ran = np.array(ran)
		ran_height		= ran.max()-ran.min()
		ymin			= ran.min()-.20*ran_height
		ymax			= ran.max()+.20*ran_height
		xmin 			= dom.min()
		xmax 			= dom.max()
		if self.debugging == True:
			print "min,max:",ran.min(),",",ran.max()
			print self
		

		plt.plot(dom,ran,'b-')
		plt.grid(which='major',axis='both',color='g')
		plt.xlim(xmin,xmax)
		plt.ylim(-.25,1.25)
		plt.ylabel("Pertenencia")
		plt.title(self.name)
		plt.show()



	def __repr__ (self,x=0):
		for item in self.__dict__:
			print(item," : ",self.__dict__[item])
		return ""



