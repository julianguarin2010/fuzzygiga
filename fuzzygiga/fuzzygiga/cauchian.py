# -*- coding: utf-8 -*-
import bell
import sys
import math



class cauchian(bell.bell):

	

	def mbfx(self,x):
		den	= 1 + (abs((x-self.center))**(2*self.narrow))
		y	= self.height/den 
		val = super(cauchian,self).mbfx(y,x)
		assert(x!=None)
		#if self._evaluation_type == 'sugeno':
		#		print x,"-->",y,"-->",val
		return val
		

	
	
	def __init__(self, center=0.0, narrow = 3.0, height = 1.0, name='cauchian'):
		super(cauchian,self).__init__(
			center,
			narrow,
			height,
			name
		)
		

if __name__ == '__main__':

	c = cauchian(name = 'cauchian')
	c.fastplot()
	
	c = cauchian (name = 'cauchian sugeno w = 0.2')
	c.w = 0.2
	c.evaluation_type = 'sugeno'
	c.fastplot()

	c = cauchian (name = 'cauchian yager w = 2.1')
	c.w = 2.1
	c.evaluation_type = 'yager'
	c.fastplot()
	

