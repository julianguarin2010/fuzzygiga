# -*- coding: utf-8 -*-
import fuzzygiga
from fuzzygiga import rect, domran, pico, trapecio,intersection,union,mbfx
import numpy as np 
import matplotlib.pyplot as plt 



help(fuzzygiga)

#RECTA0 & RECTA1
r0 = rect.rect(name='Recta0')
r0.fastplot()



r1 = rect.rect(pendant = -4.0, zero=10.0,name = 'Recta 1')
r1.fastplot()


#FORMAR UN TRAPECIO USANDO LA INTERSECCION DE LAS DOS RECTAS
r0_intersectado_r1 = intersection.fuzzyintersection(r0,r1,name = 'Interseccion Rectas 0 1')
r0_intersectado_r1.fastplot()



#FUNCION PICO
p0 = pico.pico(narrow = 1.25,center = -8)
p0.name = "Pico 0"
p0.fastplot()

#FUNCION TRAPECIO USANDO EL MODULO DE TRAPECIO
trap0 = trapecio.trapecio(offset = 12.0, b1 = 2.0, width = 4.0, b2 = 3.0,name = "Trapecio 0")
trap0.fastplot()


#INTERSECCION TRAPECIO Y PICO
trap0_intersectado_p0 = intersection.fuzzyintersection(trap0,p0,name = "Interseccion Trapecio y Pico")
trap0_intersectado_p0.fastplot()


trap0_union_p0 = union.fuzzyunion(trap0,p0,name ="Union")
trap0_union_p0.fastplot()