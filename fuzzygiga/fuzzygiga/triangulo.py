import mbfx
import intersection
import rect
import sys

class triangulo(intersection.fuzzyintersection):


	@property
	def b1(self):
		return self._b1
	
	@b1.setter
	def b1(self,value):
		if value == 0:
			value = sys.float_info.min 
		self.xmin = self.center - value
		self._b1 = value
		r1 = self.mbfx_tuple[0]
		r1.pendant = 1.0/value
		r1.zero = self.xmin

	
	@property
	def b2(self):
		return self._b2
	
	@b2.setter
	def b2(self,value):
		if value == 0:
			value = sys.float_info.min 
		self.xmax = self.center + value
		self._b2 = value
		r2 = self.mbfx_tuple[1]
		r2.pendant = -1.0/value
		r2.zero = xmax

	@property
	def center(self):
		return self._center
	
	@center.setter
	def center(self,value):
		self._center = value
		self.b1 = self.b1
		self.b2 = self.b2

	def __init__(self, center = 0.0, b1=1.0, b2=1.0, name = 'triangle'):


		r1 = rect.rect(center - b1,1.0/b1,name=name+'Recta0')
		r2 = rect.rect(center+b2,-1.0/b2,name=name+'Recta1')
		super(triangulo,self).__init__(r1,r2,name=name)

#TEST

if __name__ == '__main__':
	import numpy as np
	t = triangulo(center = 5, b1 = 3, b2 = 4)
	t.fastplot()
	

