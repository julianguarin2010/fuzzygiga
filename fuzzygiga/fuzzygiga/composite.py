# -*- coding: utf-8 -*-
import sys
import numpy as np
import matplotlib as plt
import mbfx


class compositefunction(mbfx.mbfx):

	@property
	def mbfx_tuple(self):
		return self._mbfx_tuple
	@mbfx_tuple.setter
	def mbfx_tuple(self,values):
		assert(values!=())
		self._mbfx_tuple = values
	
	@property
	def composition_lambda(self):
	    return self._composition_lambda
	@composition_lambda.setter
	def composition_lambda(self,value):
		self._composition_lambda = value


	def mbfx(self, x):
		values = []
		for fx in self.mbfx_tuple:
			values.append(fx.mbfx(x))
 
		return super(compositefunction,self).mbfx(self.composition_lambda(values),x)
	
	def __init__(self, *args):

		fuzzysets = args[0]
		arguments_dictionary = args[1]
		
		#Dictionary arguments
		if 'name' not in arguments_dictionary:
			arguments_dictionary['name']='composite'
		name = arguments_dictionary['name']

		#Establecer la funcion
		self.mbfx_tuple = fuzzysets

		xmax = sys.float_info.min
		xmin = sys.float_info.max
		if self.mbfx_tuple == ():
			xmax = 1.0
			xmin = 0.0
		else:
			for mf in self.mbfx_tuple:
				if mf.xmin < xmin:
					xmin = mf.xmin
				if mf.xmax > xmax:
					xmax = mf.xmax


		super(compositefunction,self).__init__(xmin,xmax,name)


	




	





