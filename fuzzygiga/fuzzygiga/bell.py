# -*- coding: utf-8 -*-
import mbfx
import sys
import math

min_narrow = 0.000001

class bell(mbfx.mbfx):

	@property
	def bound(self):
	    return self._bound
	
	
	@property
	def center(self):
		return self._center
	@center.setter
	def center(self, value):
		self._center = value
		#bound = math.pi/self.narrow
		self.xmin = (self._center-self.narrow)
		self.xmax = (self._center+self.narrow)


	@property
	def height(self):
		return self._height
	@height.setter 
	def height(self, value):
		self._height = value


	@property
	def narrow(self):
	    return self._narrow

	@narrow.setter
	def narrow(self, value):
		
		if value == 0.0:
			value = min_narrow 
		
		if value < 0.0:
			value = -value
		
		self._narrow = value
		try:
			self.center = self.center #Acomodar xmin y xmax
		except:
			pass 

	def mbfx(self,y,x):
		return super(bell,self).mbfx(y,x)
		

	def __init__(self, center=0.0, narrow=1.0, height=1.0, name='bell'):

		self.narrow			= narrow
		self.center			= center
		self.height			= height

		super(bell,self).__init__(self.xmin,self.xmax,name)
		self.dominion_feature=['center']
		
