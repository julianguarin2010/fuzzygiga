# -*- coding: utf-8 -*-
import mbfx
import sys
import math



class sigmoid(mbfx.mbfx):

	@property
	def center(self):
		return self._center
	@center.setter
	def center(self, value):
		self._center = value

	@property
	def pendant(self):
		return self._pendant
	@pendant.setter
	def pendant(self,value):
		if value == 0:
			value = 0.00001
		self._pendant = value
		self.xmin = self.center - 5/value
		self.xmax = self.center + 5/value


	@property
	def height(self):
		return self._height
	@height.setter 
	def height(self, value):
		self._height = value


	def mbfx(self,x):

		
		den = 1 + math.e**(self._pendant*(self._center-x))
		return super(sigmoid,self).mbfx(self._height/den,x)

	
	
	def __init__(self, center=1.0, pendant = 1.0, height = 1.0, name = 'sigmoid'):
		
		super(sigmoid,self).__init__(name=name)
		self.dominion_feature=['center']
		self.center		= center 
		self.pendant	= pendant
		self.height		= height


if __name__ == '__main__':

	s = sigmoid(name = 'sigmoid normal')
	s.fastplot()


	s = sigmoid(name = 'sigmoid normal sugeno w = 0.2')
	s.evaluation_type = 'sugeno'
	s.w = 0.2
	s.fastplot()

	s = sigmoid(name = 'sigmoid normal yager w = 1.8')
	s.evaluation_type = 'yager'
	s.w = 1.8	
	s.fastplot()
	


