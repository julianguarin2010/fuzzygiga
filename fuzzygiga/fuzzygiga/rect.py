# -*- coding: utf-8 -*-
import mbfx
import sys


class rect(mbfx.mbfx):
	
	"""
	rect.rect (zero=0.0, pendant=1.0):
	zero : Coreto por cero en el eje x.
	pendant : Pendiente de la recta

	"""

	@property
	def pendant(self):
		return self._pendant
	@pendant.setter
	def pendant(self, value):
		self._pendant = value
	
	@property
	def zero(self):
	    return self._zero
	
	@zero.setter
	def zero(self,value):
		self._zero = value
	
	def mbfx(self,x):

		return super(rect,self).mbfx((x - self.zero) * self._pendant,x)
	
	def __init__ (self, zero=0.0, pendant=1.0 , name = 'rect'):
		
		#x zero: Valor en el que la recta vale 0, por donde la recta corta el eje x
		if pendant == 0.0:
			pendant = sys.float_info.min
			xmin = zero
			xmax = zero
		elif pendant>0:
			xmin = zero
			xmax = 1.0/pendant + zero
		else:
			xmin = 1.0/pendant + zero 
			xmax = zero

		width = xmax-xmin

		super(rect,self).__init__(
			xmin,
			xmax,
			name = name)
		self.dominion_feature=['zero']
		
		self.pendant	= pendant
		self.zero		= zero

if __name__ == '__main__':
	import union
	import intersection
	a =  rect(4.0,1.0, name = 'recta a normal')
	b = rect(1.0,-.25)
	u = union.fuzzyunion(a,b,name = ' union of two rects, normal evaluation').fastplot()
	a.evaluation_type = 'sugeno'
	a.w = 0.2
	b.evaluation_type = 'sugeno'
	b.w = 0.2
	u_ = intersection.fuzzyintersection(a,b,name = 'intersection of a sugeno w = 0.2 rect and a yager w=2.1 rect' ).fastplot()
	




	