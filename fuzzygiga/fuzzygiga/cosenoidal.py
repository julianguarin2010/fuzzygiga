# -*- coding: utf-8 -*-
import bell
import sys
import math



class cosenoidal(bell.bell):

	


	@property
	def amplitude(self):
	    return self._amplitude
	@amplitude.setter
	def amplitude(self,value):
		self._amplitude = value

	
	

	def mbfx(self,x):

		y = self.amplitude * (1 + math.cos((x - self.center)*math.pi/self.narrow))
		if (x < (self.center-self.narrow)) or (x > (self.center+self.narrow)):
			y = 0
		return super(cosenoidal,self).mbfx(y,x)
		
	
	
	def __init__(self, center=0, narrow = 1, height = 1.0, name = 'cosenoidal'):
		
		#Center, es la coordenada donde estará el centro del pico
		#Narrow, es una medida de lo angosto que será el pico
		
		self.amplitude	= 0.5*height
		
		super(cosenoidal,self).__init__(
			center,
			narrow,
			height,
			'cosenoidal' 
		)

		

		




if __name__ == '__main__':

	c = cosenoidal()
	c.fastplot()
	
	c = cosenoidal()
	c.evaluation_type='sugeno'
	c.w = 0.3
	c.debugging=True
	c.fastplot()

	c = cosenoidal()
	c.evaluation_type='yager'
	c.w = 0.2
	c.debugging=True
	c.fastplot()
	




