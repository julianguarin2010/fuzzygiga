# -*- coding: utf-8 -*-
import bell
import sys
import math


class pico(bell.bell):


	def mbfx(self,x):
		exponente	= -abs(self.narrow*(x-self.center))
		y			= self.height * (math.e**exponente)
		return super(pico,self).mbfx(y,x)

	def __init__(self, center = 0.0, narrow = 2.5, height = 1.0, name = 'peak'):
		
		super(pico,self).__init__(
			center,
			narrow,
			height,
			name			
			)





if __name__ == '__main__':
	

	p = pico(name = 'pico normal')
	p.fastplot()


	p = pico(name = 'pico normal sugeno w = 0.2')
	p.evaluation_type = 'sugeno'
	p.w = 0.2
	p.fastplot()

	p = pico(name = 'pico normal yager w = 1.8')
	p.evaluation_type = 'yager'
	p.w = 1.8	
	p.fastplot()



			
	
