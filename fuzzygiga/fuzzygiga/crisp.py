# -*- coding: utf-8 -*-
import mbfx


 
class crisp(mbfx.mbfx):

	@property
	def right_bound(self):
	    return self._right_bound

	@property
	def left_bound(self):
	    return self._left_bound



	@property
	def center(self):
	    return self._center
	@center.setter
	def center(self,value):
		self._center = value
		self._left_bound = value - self._width * .5
		self._right_bound = value + self._width * .5



	@property
	def width(self):
		return self._width
	@width.setter
	def width(self,value):
		self._width = value

	def mbfx(self,x):
		return super(crisp,self).mbfx((x>=self.left_bound) and (x<=self.right_bound))

	
	def __init__(self, center=0.0, width = 1.0, name = 'classic'):
		#x OFFSET: Menor valor que pertenece al conjunto
		self.width = width
		self.center = center
		super(crisp,self).__init__(
			center-width*.5-.20*width,
			center+width*.5+.20*width,
			name)
		

	
if __name__=='__main__':
	
	crisp(name = 'Classic Normal Set').fastplot()

	c = crisp(name = 'Classic Sugeno w=0.2')
	c.w = 0.2
	c.evaluation_type = 'sugeno'
	c.fastplot()	

	c = crisp(name = 'Classic yager w=1.7')
	c.w = 1.7
	c.evaluation_type = 'yager'
	c.fastplot()	
		



