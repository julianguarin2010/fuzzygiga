import mbfx
import intersection
import rect
import sys

class trapecio(intersection.fuzzyintersection):


	@property
	def b1(self):
		return self._b1
	
	@b1.setter
	def b1(self,value):
		self._b1 = value
	
	@property
	def b2(self):
		return self._b2
	
	@b2.setter
	def b2(self,value):
		self._b2 = value
	
	@property
	def width(self):
		return self._width
	
	@width.setter
	def width(self,value):
		self._width = value


	def __init__(self, offset = 0.0, b1=1.0, width=3.0, b2=1.0, name = 'trapezium'):
		if b1 == 0:
			b1 = sys.float_info.min 
		if b2 == 0: 
			b2 = sys.float_info.min

		r0 = rect.rect(offset,1.0/b1,name=name+'Recta0')
		r1 = rect.rect(offset+b1+b2+width,-1.0/b2,name=name+'Recta1')
		super(trapecio,self).__init__(r0,r1,name=name)



#TEST

if __name__ == '__main__':
	import numpy as np
	t = trapecio(offset = 30.0, b1=0.0, width=8.0, b2=6.3)
	t.fastplot()
	print(t.__dict__)

