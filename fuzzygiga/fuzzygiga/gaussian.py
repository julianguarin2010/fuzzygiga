# -*- coding: utf-8 -*-
import bell
import sys
import math



class gaussian(bell.bell):

	
	
	@property
	def devstd(self):
	    return self._devstd
	
	@property
	def narrow(self):
	    return self._narrow
	
	@narrow.setter
	def narrow(self,value):
		super(gaussian,self.__class__).narrow.fset(self,value)
		self._devstd=self.narrow/3
	
	def mbfx(self,x):
		
		exponente = -(((x-self.center)**2)/(2*(self.devstd**2)))
		y = math.e**exponente
		return super(gaussian,self).mbfx(y,x)
	
	def __init__(self, center=0.0, narrow = 0.1, height = 1.0, name = 'gaussian'):

		super(gaussian,self).__init__(
			center,
			narrow,
			height,
			'gaussian'
		)

		

		




if __name__ == '__main__':

	g = gaussian(name = 'gaussian')
	g.fastplot()

	g = gaussian(name = 'gaussian sugeno w = 0.2')
	g.w = 0.2
	g.evaluation_type = 'sugeno'
	g.fastplot()

	g = gaussian(name = 'gaussian yager w = 2.3')
	g.w = 2.3
	g.evaluation_type = 'yager'
	g.fastplot()

