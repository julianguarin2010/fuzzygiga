# -*- coding: utf-8 -*-
import composite

class fuzzyunion(composite.compositefunction):

	def __init__(self, *fuzzysets, **arguments_dictionary):
		super(fuzzyunion,self).__init__(fuzzysets,arguments_dictionary)
		
		self.composition_lambda = lambda x: max(x) 



if __name__ == '__main__':
	import pico
	import trapecio	
	#FUNCION PICO
	p0 = pico.pico(narrow = 1.25,center = 0)
	p0.name = "Pico0"
	p0.fastplot()

	#FUNCION TRAPECIO USANDO EL MODULO DE TRAPECIO
	trap0 = trapecio.trapecio(offset = 12.0, b1 = 2.0, width = 4.0, b2 = 3.0,name = "Trapecio0")
	trap0.fastplot()


	#INTERSECCION TRAPECIO Y PICO
	trap0_intersectado_p0 = fuzzyunion(trap0,p0,name = "Union Trapecio y Pico")
	trap0_intersectado_p0.fastplot()


	trap0_intersectado_p0.evaluation_type = 'sugeno'
	trap0_intersectado_p0.name = 'Union Trapecio y Pico sugeno w = 0.2'
	trap0_intersectado_p0.w = 0.2
	trap0_intersectado_p0.fastplot()

	trap0_intersectado_p0.evaluation_type = 'yager'
	trap0_intersectado_p0.name = 'Union Trapecio y Pico yager w = 2.1'
	trap0_intersectado_p0.w = 2.1
	trap0_intersectado_p0.fastplot()
