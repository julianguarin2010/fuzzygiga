# -*- coding: utf-8 -*-
import mbfx
import sys
import math



class concavoidal(mbfx.mbfx):

	@property
	def concavaoidal_lambda(self):
	    return self._concavoidal_lambda
	

	@property
	def narrow(self):
	    return self._narrow
	@narrow.setter
	def narrow(self,value):
		self._narrow = value
	@property
	def top(self):
	    return self._top
	@top.setter
	def top(self,value):
		self._top = value

	@property
	def height(self):
		return self._height
	@height.setter
	def height(self, value):
		self._height = value

	@property
	def creciente(self):
	    return self._creciente
	@creciente.setter
	def creciente(self, value):
		
		try: 
			top		= self.top
			height	= self.height
		except:
			top		= 0.0
			height	= 0.0

		if value == True:
			self._creciente				= True
			self.narrow					= self.top - self.narrow
			self._concavoidal_lambda	= lambda x : height * (self._top - self._narrow) / ((2*self._top-self._narrow-x)**4)
		elif value == False:
			self._creciente				= False
			self.narrow 				= self.top + self.narrow 
			self._concavoidal_lambda	= lambda x : height * (self._narrow - self._top) / ((self._narrow - 2*self._top + x)**4)
		else:
			self.creciente 				= True





		

	def mbfx(self,x):

		return super(concavoidal,self).mbfx(self._concavoidal_lambda(x),x)
		
	
	
	def __init__(self, top = 6, narrow = .95 ,  height = 1.0, creciente = True, name='concave'):
			
		narrow = .99
		if narrow >= 1.0: narrow = .99
		if narrow <0.0	: narrow = .00
		narrow*=.10+.90

		#Center, es la coordenada donde estará el centro del pico
		#Narrow, es una medida de lo angosto que será el pico
		
		self.top		= top 
		self.height		= height
		self.narrow 	= (1-narrow)*top
		self.creciente	= creciente
		
		if self.creciente == True:
			delta = -0.01
			xmax = top
		else:
			delta = +0.01
			xmin = top

		lim = top+delta
		while self._concavoidal_lambda(lim)>0.01:
			lim += delta




		if self.creciente == True:	xmin 	= lim
		else: 						xmax	= lim
		width = xmax - xmin 


		print "xmin: ",xmin," xmax: ",xmax
		super(concavoidal,self).__init__(
			xmin-width*.20,
			xmax+width*.20,
			name
		)



		




if __name__ == '__main__':

	g = concavoidal(narrow = 1,creciente=True,name = 'Normal Concava')
	g.fastplot()

	
	g = concavoidal(narrow = 1,creciente=True,name = 'Sugeno Concava w = 0.2')
	g.w = 0.2
	g.debugging = True
	g.evaluation_type = 'sugeno'
	g.fastplot()

	g = concavoidal(narrow = 1,creciente=True,name = 'Yager Concava w = 0.2')
	g.w = 0.2
	g.debugging = True
	g.evaluation_type = 'yager'
	g.fastplot()

	g = concavoidal(narrow = 1,creciente=True,name = 'Yager Concava w = 1.5')
	g.w = 1.5
	g.debugging = True
	g.evaluation_type = 'yager'
	g.fastplot()

