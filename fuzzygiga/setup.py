from setuptools import setup 

setup(
	name = 'fuzzygiga',
	version = '0.1',
	description = 'A fuzzy package for python 2.7',
	url = 'https_//bitbuclet.org/julianguarin2010/fuzzygiga/wiki/Home',
	author = 'Copyright (c) 2015 Fitech Giga',
	license = 'Zlib',
	packages = ['fuzzygiga'],
	zip_safe = False)